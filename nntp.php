<?php

function nntp_connect($server,$port=119) {
  $s = @fsockopen($server,$port,$errno,$errstr,30);
  if (!$s) {
    echo "<!-- error connecting to nntp server: $errstr -->\n";
    return false;
  }
  $hello = fgets($s, 1024);
  if (substr($hello,0,4) != "200 ") {
    echo "<!-- unexpected greeting: $hello -->\n";
    return false;
  }
  return $s;
}

function nntp_cmd($conn,$command,$expected) {
  if (strlen($command) > 510) die("command too long: $command");
  fputs($conn, "$command\r\n");
  $res = fgets($conn, 1024);
  list($code,$extra) = explode(" ", $res, 2);
  return $code == $expected ? $extra : false;
}



function nntp_get_groups_on_server($server)
{
    
    
$s = nntp_connect($server);
nntp_cmd($s,"LIST",215);

$return_array=array();

while ($line = fgets($s, 1024)) {
  if ($line == ".\r\n") break;
  $line = chop($line);
  list($group,$high,$low,$active) = explode(" ", $line);
$return_array[]=$group;
  
}

return $return_array;
    
}




function nntp_get_posts_on_groups_server($server,$group)
{
 
$return_array=array();   
    
$s = nntp_connect($server);
$res = nntp_cmd($s,"GROUP $group",211);
$i=0;
list (,$f,$l,$g) = explode(" ", $res);
$i=(int)$i;
if (!$i || $i > $l - 20 || $i < $f) $i = $l - $f > 20 ? $l - 20 : $f;
$n = min($l+1,$i+20);
$res = nntp_cmd($s,"XOVER $i-$n", 224);
//    navbar($group,$f,$l,$i);
  
$charset = "";

while ($line = fgets($s, 4096)) {
  if ($line == ".\r\n") break;
  $line = chop($line);
  list($n,$subj,$author,$odate,$messageid,$references,$bytes,$lines,$extra)= explode("\t", $line, 9);
  $date822 = date("r", strtotime($odate));

$return_array[$n]['lines']=$lines;

$return_array[$n]['subject']=$subj;
$return_array[$n]['author']=$author;

    
    
}


return $return_array;
    
}



  
  function drupal_nntp_show_group_list($server)
  {
  $totalgroups=nntp_get_groups_on_server($server);

$pagecontent="<ul>";

foreach ($totalgroups as $group)
    {
       $pagecontent .= "<li>";
      $pagecontent .= "<a href='/nntppage/";
       $pagecontent .= "$server/"; 
    
    $pagecontent .= $group;
  
    $pagecontent .= "'>";
     $pagecontent .= $group;
     $pagecontent .= "</a>";
      $pagecontent .= "</li>";
    
    }
    $pagecontent .="</ul>";

return $pagecontent;    
      
      
  }
  
  
    
  function drupal_nntp_show_posts_list($server,$group)
  {
  $totalposts=nntp_get_posts_on_groups_server($server,$group);

  
  
$pagecontent="<ul>";

foreach ($totalposts as $postid=>$values)
    {
  
     $pagecontent .= "<li>";
      $pagecontent .= "<a href='/nntppage/";
       $pagecontent .= "$server/"; 
    
    $pagecontent .= "$group/$postid";
    
  
    $pagecontent .= "'>";
     $pagecontent .= $values['subject'];
     $pagecontent .= "</a>";
      $pagecontent .= "</li>";
    }
    
    
    $pagecontent .="</ul>";

return $pagecontent;    
      
      
  }
  
  
  function drupal_nntp_list_servers()
  {
      
    $nodes = node_load_multiple(array(), array('type' => "nntp_servers"));  
   
    $pagecontent ="<ul>";
    
    foreach ($nodes as $node)
        {
        
        $pagecontent .= "<li><a href='/nntppage/";
        
        $pagecontent .= $node->field_nntp_server_url['und']['0']['value'];
        $pagecontent .= "'>";
     $pagecontent .= $node->title;
 
      $pagecontent .= "</a>";
    $pagecontent .= "</li>";
    
    }
    
    
    $pagecontent .="</ul>";
    
    
    
      return $pagecontent;
      
  }
  
  
  
 function  drupal_nntp_show_posts_details($server,$group,$postid)
                {
$s = nntp_connect($server);
if ($group) {
  $res = nntp_cmd($s,"GROUP $group",211);
}

$res = nntp_cmd($s, "ARTICLE $postid",220);
$started = 0;
$inheaders = 1; $headers = array();
$masterheaders = null;
$mimetype = $boundary = $charset = $encoding = "";
$mimecount = 0; // number of mime parts
$boundaries = array();
$lk = '';
$linecontent ="";
while (!feof($s)) {
  $line = fgets($s, 4096);

 
$linecontent .= $line;



  }
  
$mailcontent= new DrupalNNTPMimeMessage($linecontent);

  
$pagecontent ="";

$pagecontent .= "<div id='drupal_nntp_post_subject' class='title'>" .$mailcontent->getSubject();
$pagecontent .="</div>";

$pagecontent .= "<div id='drupal_nntp_post_body'> <pre>" .$mailcontent->getHTMLBody();
$pagecontent .="</pre></div>";
  
return $pagecontent;
}
<?php
/**
 * @file
 * drupal_nntp_servers.features.inc
 */

/**
 * Implements hook_node_info().
 */
function drupal_nntp_servers_node_info() {
  $items = array(
    'nntp_servers' => array(
      'name' => t('NNTP Servers'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
